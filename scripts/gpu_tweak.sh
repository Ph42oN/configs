#!/bin/bash
case $1/$2 in
  pre/*)
    # Put here any commands expected to be run when suspending or hibernating.
	echo r > /sys/class/drm/card0/device/pp_od_clk_voltage
    ;;
  post/*)
    # Put here any commands expected to be run when resuming from suspension or 
    # thawing from hibernation.
	/etc/local.d/gpu_tweak.start
    ;;
esac
